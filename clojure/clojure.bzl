DEFAULT_LEININGEN_VERSION = "2.8.1"
DEFAULT_CLOJURE_VERSION = "1.9.0"
DEFAULT_SPEC_ALPHA_VERSION = "0.2.176"
DEFAULT_CORE_SPECS_ALPHA_VERSION = "0.2.44"
# LEIN_DEPS = [
  
# ]

def _lein_cmd(subcmd, jvm_flags=[], deps=[]):
  return "external/local_jdk/bin/java %s -cp %s clojure.main -m leiningen.core.main %s\n" % (
    " ".join(jvm_flags),
    ":".join([dep.short_path for dep in deps]),
    subcmd,
  )

def _clojure_uberjar_impl(ctx):
  """Creates an uberjar file from Clojure sources. Users should rely on
  clojure_library instead of using this rule directly.
  """
  class_jar = ctx.outputs.class_jar
  build_output = class_jar.path + ".build_output"

  # Extract all transitive dependencies
  all_deps = depset(ctx.files.req_deps + ctx.files.deps)
  for this_dep in ctx.attr.deps:
    if hasattr(this_dep, "java"):
      all_deps += this_dep.java.transitive_runtime_deps

  # Set up the output directory and set JAVA_HOME
  cmd = "rm -rf %s\n" % build_output
  cmd += "mkdir -p %s\n" % build_output
  cmd += "export JAVA_HOME=%s\n" % ctx.attr._jdk[java_common.JavaRuntimeInfo].java_home

  # This will work if our current working directory is a Leiningen project.
  lein_cmd = _lein_cmd(
      "uberjar",
      jvm_flags=ctx.attr.jvm_flags,
      deps=all_deps,
  )
  cmd += lein_cmd + "\n"
  # NOTE:  uberjar will create file under $proj_dir/target/*.jar
  # TODO:  If Bazel does not already, we should move this JAR to the
  #        supplied output location (class_jar).

  # Clean up temporary output
  cmd += "rm -rf %s" % build_output

  # Execute the command
  ctx.action(
      inputs = (
          ctx.files.srcs
          + list(all_deps)
          + ctx.files._jdk
      ),
      outputs = [class_jar],
      mnemonic = "leinuberjar",
      command = "set -e; " + cmd,
      use_default_shell_env = True,
  )

_clojure_uberjar = rule(
    attrs = {
        "srcs": attr.label_list(
            non_empty = True,
            allow_files = FileType([
                ".clj",
                ".java",
                ".edn",
                ".sql",
                ".xml",
                ".json",
            ]),
        ),
        "req_deps": attr.label_list(
            default = [
                       "//external:leiningen",
                       "//external:clojure",
                       "//external:spec_alpha",
                       "//external:core_specs_alpha",
                       ]
        ),
        "deps": attr.label_list(
            mandatory = False,
            # allow_files = FileType([".jar"]),
        ),

        "jvm_flags": attr.string_list(),
        "_jdk": attr.label(
            default = Label("@bazel_tools//tools/jdk:current_java_runtime"),
        ),
    },
    outputs = {
        "class_jar": "%{name}.jar",
    },
    implementation = _clojure_uberjar_impl,
)

def clojure_library(name, srcs=[], testonly=0, deps=[], **kwargs):
  """Rule analagous to java_library that accepts .clj sources instead of
  .java sources. The result is wrapped in a java_import so that java rules may
  depend on it.
  """
  _clojure_uberjar(
      name = name + "-impl",
      srcs = srcs,
      testonly = testonly,
      deps = deps,
  )
  native.java_import(
      name = name,
      jars = [name + "-impl"],
      testonly = testonly,
      deps = deps,
      **kwargs
  )

def clojure_binary(name, main_class, srcs=[], testonly=0, deps=[], **kwargs):
  """Rule analagous to java_binary that accepts .clj sources instead of .java
  sources.
  """
  ## RWN: Seems like `lein uberjar` is what I would want here.
  all_deps = deps + [] # ["//external:clojure", "//external:leiningen"]
  if srcs:
    clojure_library(
        name = name + "-lib",
        srcs = srcs,
        testonly = testonly,
        deps = deps,
    )
    all_deps += [name + "-lib"]

  native.java_binary(
      name = name,
      main_class = main_class,
      runtime_deps = all_deps,
      testonly = testonly,
      **kwargs
  )

def _clojure_test_impl(ctx):
  # Extract all transitive dependencies
  all_deps = depset(ctx.files.deps + ctx.files._implicit_deps)
  for this_dep in ctx.attr.deps:
    if hasattr(this_dep, 'java'):
      all_deps += this_dep.java.transitive_runtime_deps

  # Write a file that executes `lein test`
  cmd = _lein_cmd(
    "test",
    jvm_flags=ctx.attr.jvm_flags,
    deps=all_deps,
  )
  ctx.file_action(
    output = ctx.outputs.executable,
    content = cmd
  )

  # Return all dependencies needed to run the tests
  return struct(
    runfiles=ctx.runfiles(files=list(all_deps) + ctx.files.data + ctx.files._jdk),
  )

# java -cp ~/.lein/self-installs/leiningen-2.8.1-standalone.jar:~/.m2/repository/org/clojure/clojure/1.9.0/clojure-1.9.0.jar clojure.main -m leiningen.core.main test
_clojure_test = rule(
    attrs = {
        "srcs": attr.label_list(
            mandatory = True,
            allow_files = FileType([".clj"]),
        ),
        "deps": attr.label_list(
            allow_files = FileType([".jar"])
        ),
        "data": attr.label_list(allow_files = True),
        "jvm_flags": attr.string_list(),
        "_jdk": attr.label(
            default = Label("@bazel_tools//tools/jdk:current_java_runtime"),
        ),
        "_implicit_deps": attr.label_list(
            default = [
                "//external:clojure_artifact",
                "//external:leiningen_artifact",
            ]),
    },
    test = True,
    implementation = _clojure_test_impl,
)

def clojure_test(
    name,
    deps=[],
    srcs=[],
    data=[],
    resources=[],
    jvm_flags=[],
    size="medium",
    tags=[]):
  # Create an extra jar to hold the resource files if any were specified
  all_deps = deps
  if resources:
    native.java_library(
      name = name + "-resources",
      resources = resources,
      testonly = 1,
    )
    all_deps += [name + "-resources"]

  _clojure_test(
    name = name,
    size = size,
    tags = tags,
    srcs = srcs,
    deps = all_deps,
    data = data,
    jvm_flags = jvm_flags,
  )

def clojure_repositories(
    leiningen_version = DEFAULT_LEININGEN_VERSION,
    clojure_version = DEFAULT_CLOJURE_VERSION,
    spec_alpha_version = DEFAULT_SPEC_ALPHA_VERSION,
    core_specs_alpha_version = DEFAULT_CORE_SPECS_ALPHA_VERSION):
  native.maven_server(
    name = "maven",
    url = "https://repo1.maven.org/maven2/",
  )

  # http://clojars.org/repo/leiningen/leiningen/2.8.1/leiningen-2.8.1.jar
  native.maven_server(
    name = "clojars",
    url = "http://clojars.org/repo/",
  )

  native.maven_jar(
    name = "clojure_artifact",
    artifact = "org.clojure:clojure:%s" % (clojure_version),
    server = "maven",
  )
  native.bind(
    name = "clojure",
    actual = "@clojure_artifact//jar",
  )

  # Lame, but necessary for 1.9.0
  native.maven_jar(
    name = "spec_alpha_artifact",
    artifact = "org.clojure:spec.alpha:%s" % (spec_alpha_version),
    server = "maven",
  )
  native.bind(
    name = "spec_alpha",
    actual = "@spec_alpha_artifact//jar",
  )
  native.maven_jar(
    name = "core_specs_alpha_artifact",
    artifact = "org.clojure:core.specs.alpha:%s" % (core_specs_alpha_version),
    server = "maven",
  )
  native.bind(
    name = "core_specs_alpha",
    actual = "@core_specs_alpha_artifact//jar",
  )

  native.maven_jar(
    name = "leiningen_artifact",
    artifact = "leiningen:leiningen:%s" % (leiningen_version),
    server = "clojars",
  )
  native.bind(
    name = "leiningen",
    actual = "@leiningen_artifact//jar"
  )
